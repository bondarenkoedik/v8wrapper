#include <map>
#include <string>
#include <iostream>

#include <stdlib.h>
#include <string.h>

#include <v8.h>
#include <libplatform/libplatform.h>

using namespace std;
using namespace v8;

void ParseOptions(int argc, char* argv[], std::map<std::string, std::string>* options, std::string* file);
v8::MaybeLocal<v8::String> ReadFile(v8::Isolate* isolate, const std::string& name);

bool v8_exec(const Local<String>& source, Isolate* isolate, v8::Handle<v8::ObjectTemplate> global);
std::string json_str(v8::Isolate* isolate, v8::Handle<v8::Value> value);
v8::Local<v8::Object> str_json(v8::Isolate* isolate, Handle<Value> value);
Local<Object> convertToObject(v8::Isolate* isolate, Local<Object> json);
void Print(const v8::FunctionCallbackInfo<v8::Value>& args);


int main(int argc, char* argv[]) {

  map<string, string> options;
  string file;
  ParseOptions(argc, argv, &options, &file);
  if (file.empty()) {
    fprintf(stderr, "No script was specified.\n");
    return 1;
  }

  v8::V8::InitializeICUDefaultLocation(argv[0]);
  v8::V8::InitializeExternalStartupData(argv[0]);
  v8::Platform* platform = v8::platform::CreateDefaultPlatform();
  v8::V8::InitializePlatform(platform);
  v8::V8::Initialize();

  Isolate::CreateParams create_params;
  create_params.array_buffer_allocator =
      v8::ArrayBuffer::Allocator::NewDefaultAllocator();

  Isolate* isolate =  Isolate::New(create_params);
  Isolate::Scope isolate_scope(isolate);
  HandleScope scope(isolate);

  v8::Handle<v8::ObjectTemplate> global = v8::ObjectTemplate::New(isolate);
  global->Set(v8::String::NewFromUtf8(isolate, "print"), v8::FunctionTemplate::New(isolate, Print));

  Local<String> source;
  if (!ReadFile(isolate, file).ToLocal(&source)) {
    fprintf(stderr, "Error reading '%s'.\n", file.c_str());
    return 1;
  }

  v8_exec(source, isolate, global);
}

bool v8_exec(const Local<String>& source, Isolate* isolate, v8::Handle<v8::ObjectTemplate> global)
{

  // Isolate::Scope isolate_scope(isolate);

  // Create a stack-allocated handle scope.
  // HandleScope handle_scope(isolate);

  // Create a new context.
  Local<Context> context = Context::New(isolate, NULL, global);

  // Enter the context for compiling and running the hello world script.
  Context::Scope context_scope(context);

  TryCatch try_catch(isolate);

  Local<Script> compiled_script;
  if (!Script::Compile(context, source).ToLocal(&compiled_script)) {
    String::Utf8Value error(try_catch.Exception());
    printf("Logged: %s\n", *error);
    // The script failed to compile; bail out.
    return false;
  }

  // Run the script!
  Local<Value> result;
  if (!compiled_script->Run(context).ToLocal(&result)) {
    // The TryCatch above is still in effect and will have caught the error.
    String::Utf8Value error(try_catch.Exception());
    printf("Logged: %s\n", *error);
    // Running the script failed; bail out.
    return false;
  }

  Local<Function> function = Local<Function>::Cast(result);

  string str = "{\"a\": 1, \"b\": 2, \"arr\": [1, 2, 3, \"Name\"], \"obj\": {\"a\": 100}}";

  MaybeLocal<Value> data = v8::JSON::Parse(isolate, v8::String::NewFromUtf8(isolate, str.c_str()));
  if (data.IsEmpty()) {
    std::cout << "error during parse JSON" << std::endl;
    return 1;
  }

  Local<Object> obj = data.ToLocalChecked()->ToObject();

  Local<Value> args[] = { convertToObject(isolate, obj) };
  Local<Value> res = function->Call(context->Global(), 1, args);
  // Local<Value> res = function->Call(function, 1, args);

  std::cout << json_str(isolate, res) << std::endl;

}


/// Stringify V8 value to JSON
/// return empty string for empty value
std::string json_str(v8::Isolate* isolate, v8::Handle<v8::Value> value)
{
    if (value.IsEmpty())
    {
        return std::string();
    }

    // v8::HandleScope scope(isolate); // Why use it ?

    v8::Local<v8::Object> json = isolate->GetCurrentContext()->
        Global()->Get(v8::String::NewFromUtf8(isolate, "JSON"))->ToObject();
    v8::Local<v8::Function> stringify = json->Get(v8::String::NewFromUtf8(isolate, "stringify")).As<v8::Function>();

    v8::Local<v8::Value> result = stringify->Call(json, 1, &value);
    v8::String::Utf8Value const str(result);

    return std::string(*str, str.length());
 }

Local<Object> convertToObject(v8::Isolate* isolate, Local<Object> json) {

  Local<Object> obj = v8::Object::New(isolate);

  MaybeLocal<Array> maybe_props = json->GetOwnPropertyNames(isolate->GetCurrentContext());
  if (!maybe_props.IsEmpty()) {
      Local<Array> props = maybe_props.ToLocalChecked();
      for(uint32_t i=0; i < props->Length(); i++) {
         Local<Value> key = props->Get(i);
         Local<Value> value = json->Get(key);
         obj->Set(key, value);
      }
  }

  return obj;
}

// The callback that is invoked by v8 whenever the JavaScript 'print'
// function is called.  Prints its arguments on stdout separated by
// spaces and ending with a newline.
void Print(const v8::FunctionCallbackInfo<v8::Value>& args) {

    v8::Isolate *isolate = args.GetIsolate();
    v8::HandleScope handle_scope(isolate); // Why use it ?

    bool first = true;
    for (int i = 0; i < args.Length(); i++) {
        if (first)
            first = false;
        else
            printf(" ");

        if (args[i]->IsObject()) {
          std::cout << json_str(isolate, args[i]);
        } else {
          String::Utf8Value str(args[i]);
          // const char* cstr = ToCString(str);
          printf("%s", *str);
        }
    }

    printf("\n");
}

void ParseOptions(int argc,
                  char* argv[],
                  map<string, string>* options,
                  string* file) {
  for (int i = 1; i < argc; i++) {
    string arg = argv[i];
    size_t index = arg.find('=', 0);
    if (index == string::npos) {
      *file = arg;
    } else {
      string key = arg.substr(0, index);
      string value = arg.substr(index+1);
      (*options)[key] = value;
    }
  }
}

// Reads a file into a v8 string.
MaybeLocal<String> ReadFile(Isolate* isolate, const string& name) {
  FILE* file = fopen(name.c_str(), "rb");
  if (file == NULL) return MaybeLocal<String>();

  fseek(file, 0, SEEK_END);
  size_t size = ftell(file);
  rewind(file);

  char* chars = new char[size + 1];
  chars[size] = '\0';
  for (size_t i = 0; i < size;) {
    i += fread(&chars[i], 1, size - i, file);
    if (ferror(file)) {
      fclose(file);
      return MaybeLocal<String>();
    }
  }
  fclose(file);
  MaybeLocal<String> result = String::NewFromUtf8(
      isolate, chars, NewStringType::kNormal, static_cast<int>(size));
  delete[] chars;
  return result;
}
