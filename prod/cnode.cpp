/* cnode_s2.c */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <sstream>

#include "erl_interface.h"
#include "ei.h"

#include "threadpool.h"

#include "v8wrap.h"

#define BUFSIZE 1000

std::mutex g_display_mutex;

int my_listen(int);
int foo(int, int);
int bar(int);

void process(int fd, ErlMessage* emsg) {

  ETERM *fromp, *tuplep, *func, *conv_id_term, *node_id_term, *src_term, *resp;

  fromp = erl_element(2, emsg->msg);
  tuplep = erl_element(3, emsg->msg);

  func = erl_element(1, tuplep);

  conv_id_term = erl_element(2, tuplep);
  node_id_term = erl_element(3, tuplep);
  src_term = erl_element(4, tuplep);

  char* conv_id_c = erl_iolist_to_string(conv_id_term);
  char* node_id_c = erl_iolist_to_string(node_id_term);
  char* src_c = erl_iolist_to_string(src_term);

  printf("func: %s\n", ERL_ATOM_PTR(func));
  printf("conv_id_c: %s\n", conv_id_c);
  printf("node_id_c: %s\n", node_id_c);
  printf("src_c: %s\n", src_c);

  if (strncmp(ERL_ATOM_PTR(func), "compile", 7) == 0) {
    printf("in compile\n");
    char* error = NULL;
    if (!compile(conv_id_c, node_id_c, src_c, error)) {
        printf("Compile error: %s\n", error);
        delete error;
    }
  } else if (strncmp(ERL_ATOM_PTR(func), "run", 3) == 0) {
    printf("in run\n");
    char* error = NULL;
    char* res = run(conv_id_c, node_id_c, src_c, error);
    if (error) {
      printf("Compile error: %s\n", error);
      delete error;
    } else {
      printf("Result: %s\n", res);
      delete res;
    }
  }

  int res = 10;

  resp = erl_format("{cnode, ~i}", res);
  erl_send(fd, fromp, resp);

  erl_free_term(emsg->from); erl_free_term(emsg->msg);
  erl_free_term(fromp); erl_free_term(tuplep);
  erl_free_term(func);
  erl_free_term(conv_id_term); erl_free_term(node_id_term); erl_free_term(src_term);
  erl_free(conv_id_c); erl_free(node_id_c); erl_free(src_c);
  erl_free_term(resp);

  delete emsg;

  // printf("in process");

 // std::thread::id this_id = std::this_thread::get_id();
 // g_display_mutex.lock();
 // std::cout << "This thread id : " << this_id << std::endl;
 // g_display_mutex.unlock();

 // std::stringstream ss;
 // ss << this_id;

 // char* error = NULL;
 // if (!compile(ss.str().c_str(), ss.str().c_str(), "(function(data) { data.a += 1; data.b.push(1); return data; })", error)) {
 //     std::cout << "Error" << std::endl;
 //    //  return 1;
 // }

 // const char* data3 = run(ss.str().c_str(), ss.str().c_str(), "{\"a\": 1, \"b\": [1,3]}", error);

  // int res = 0;

  // fromp = erl_element(2, emsg.msg);
  // tuplep = erl_element(3, emsg.msg);
  // fnp = erl_element(1, tuplep);
  // argp = erl_element(2, tuplep);
  // argp2 = erl_element(3, tuplep);
  //
  // if (strncmp(ERL_ATOM_PTR(fnp), "foo", 3) == 0) {
  //   res = foo(ERL_INT_VALUE(argp), ERL_INT_VALUE(argp2));
  // } else if (strncmp(ERL_ATOM_PTR(fnp), "bar", 3) == 0) {
  //   res = bar(ERL_INT_VALUE(argp));
  // }

  // res = 10;
  //
  // resp = erl_format("{cnode, ~i}", res);
  // erl_send(fd, fromp, resp);
  //
  // erl_free_term(emsg.from); erl_free_term(emsg.msg);
  // erl_free_term(fromp); erl_free_term(tuplep);
  // erl_free_term(fnp); erl_free_term(argp);
  // erl_free_term(resp);

  // std::this_thread::sleep_for (std::chrono::seconds(1));
}

int main(int argc, char **argv) {

  std::string pathToLibs("../"); // absolute path here
  initialize(argc, argv, pathToLibs.c_str());

  struct in_addr addr;                     /* 32-bit IP number of host */
  int port;                                /* Listen port number */
  int listen;                              /* Listen socket */
  int fd;                                  /* fd to Erlang node */
  ErlConnect conn;                         /* Connection data */

  int loop = 1;                            /* Loop flag */
  int got;                                 /* Result of receive */
  unsigned char buf[BUFSIZE];              /* Buffer for incoming message */
  // ErlMessage emsg;                         /* Incoming message */

  ETERM *fromp, *tuplep, *func, *conv_id_term, *node_id_term, *src_term, *resp;
  int res;

  port = atoi(argv[1]);

  erl_init(NULL, 0);

  addr.s_addr = inet_addr("192.168.0.101");
  if (erl_connect_xinit("idril", "cnode", "cnode@192.168.0.101",
			&addr, "secretcookie", 0) == -1)
    erl_err_quit("erl_connect_xinit");

  /* Make a listen socket */
  if ((listen = my_listen(port)) <= 0)
    erl_err_quit("my_listen");

  if (erl_publish(port) == -1)
    erl_err_quit("erl_publish");

  if ((fd = erl_accept(listen, &conn)) == ERL_ERROR)
    erl_err_quit("erl_accept");
  fprintf(stderr, "Connected to %s\n\r", conn.nodename);

  nbsdx::concurrent::ThreadPool<std::function<void(int, ErlMessage*)>, 1> pool; // Defaults to 10 threads.

  while (loop) {

    // ErlMessage emsg;
    ErlMessage* emsg = new ErlMessage();
    got = erl_receive_msg(fd, buf, BUFSIZE, emsg);

    if (got == ERL_TICK) {
      /* ignore */
      delete emsg;
    } else if (got == ERL_ERROR) {
      loop = 0;
    } else {

      if (emsg->type == ERL_REG_SEND) {

        pool.AddJob(process, fd, emsg);

        // g_display_mutex.lock();
        // std::cout << "here" << std::endl;
        // g_display_mutex.unlock();

        // std::thread th(process, fd, emsg);
        // th.join();
        // th.detach();

      	// fromp = erl_element(2, emsg->msg);
      	// tuplep = erl_element(3, emsg->msg);
        //
        // func = erl_element(1, tuplep);
        //
        // conv_id_term = erl_element(2, tuplep);
        // node_id_term = erl_element(3, tuplep);
        // src_term = erl_element(4, tuplep);
        //
        // char* conv_id_c = erl_iolist_to_string(conv_id_term);
        // char* node_id_c = erl_iolist_to_string(node_id_term);
        // char* src_c = erl_iolist_to_string(src_term);
        //
        // printf("func: %s\n", ERL_ATOM_PTR(func));
        // printf("conv_id_c: %s\n", conv_id_c);
        // printf("node_id_c: %s\n", node_id_c);
        // printf("src_c: %s\n", src_c);

        // std::cout << conv_id_c << std::endl;
        // std::cout << node_id_c << std::endl;
        // std::cout << src_c << std::endl;

      	// fnp = erl_element(1, tuplep);
      	// argp = erl_element(2, tuplep);
        //
      	// if (strncmp(ERL_ATOM_PTR(fnp), "foo", 3) == 0) {
      	//   res = foo(ERL_INT_VALUE(argp));
      	// } else if (strncmp(ERL_ATOM_PTR(fnp), "bar", 3) == 0) {
      	//   res = bar(ERL_INT_VALUE(argp));
      	// }

        // res = 10;
        //
      	// resp = erl_format("{cnode, ~i}", res);
      	// erl_send(fd, fromp, resp);
        //
      	// erl_free_term(emsg->from); erl_free_term(emsg->msg);
      	// erl_free_term(fromp); erl_free_term(tuplep);
        // erl_free_term(func);
        // erl_free_term(conv_id_term); erl_free_term(node_id_term); erl_free_term(src_term);
        // erl_free(conv_id_c); erl_free(node_id_c); erl_free(src_c);
      	// erl_free_term(resp);
        //
        // delete emsg;
      }
    }
  }
}


int my_listen(int port) {
  int listen_fd;
  struct sockaddr_in addr;
  int on = 1;

  if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return (-1);

  setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  memset((void*) &addr, 0, (size_t) sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(listen_fd, (struct sockaddr*) &addr, sizeof(addr)) < 0)
    return (-1);

  listen(listen_fd, 5);
  return listen_fd;
}
