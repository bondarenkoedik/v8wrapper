#include <stdbool.h>
#include <ctime>
#include <omp.h>
#include <iostream>

#include <thread>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>

#include "test.h"

std::string readFile(const char* filename);

// void task(int N) {
//   for(int i = 0; i < 200; i++) {
//
//     char* err = NULL;
//     if (!compile(std::to_string(N).c_str(), std::to_string(N).c_str(), "(function(data) { for(let i = 0; i < 1000000; i++); data.a += 1; data.b.push(1); return data; })")) {
//       printf("Error\n");
//     }
//
//     // const char* data = run(std::to_string(N).c_str(), std::to_string(N).c_str(), "{\"a\": 1, \"b\": [1]}");
//     // delete[] data;
//   }
// }

int main(int argc, char* argv[]) {
  initialize(argc, argv);

  // for (int i = 1; i < 3; i++) {
  //   if (!compile(std::to_string(i).c_str(), std::to_string(i).c_str(), "(function(data) { data.a += 1; data.b.push(1); return data; })")) {
  //     std::cout << "Error" << std::endl;
  //     return 1;
  //   }
  // }

  // if (!compile("1", "1", "(function(data) { for(let i = 0; i < 1000000; i++); data.a += 1; data.b.push(1); return data; })")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }

  // if (!compile("2", "2", "(function(data) { for(let i = 0; i < 1000000; i++); data.a += 1; data.b.push(10); return data; })")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }
  //
  // if (!compile("3", "3", "(function(data) { for(let i = 0; i < 1000000; i++); data.a += 1; data.b.push(1); return data; })")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }
  //
  // if (!compile("4", "4", "(function(data) { for(let i = 0; i < 1000000; i++); data.a += 1; data.b.push(10); return data; })")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }

  // if (!compile("3", "3", "(function(data) { let sum = 0; for(let i = 0; i < 100000000; i++) sum += i; return sum;})")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }

  // if (!compile("4", "4", "(function(data) { print(data, 'hello'); print('dsad'); })" )) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }

  // std::string code = readFile("moment.js");
  // // std::cout << code << std::endl;
  // if (!compile("5", "5", "(function(data) { require('./moment.js'); print(moment().format('MMMM Do YYYY, h:mm:ss a')); })")) {
  //   std::cout << "Error" << std::endl;
  //   return 1;
  // }

  // const char* data = run("3", "3", "{}");
  // std::cout << data << std::endl;

  struct timespec start, finish;
  double elapsed;

  clock_gettime(CLOCK_MONOTONIC, &start);

  // for (int i = 0; i < 20000; i++) {
  //   const char* data = run("1", "1", "{\"a\": 1, \"b\": [1]}");
  //   // const char* data2 = run("2", "2", "{\"a\": 1, \"b\": [1,3]}");
  //   // const char* data3 = run("3", "3", "{\"a\": 1, \"b\": [1,3]}");
  //   // const char* data4 = run("4", "4", "{\"a\": 1, \"b\": [1,3]}");
  //   delete[] data;
  //   // delete[] data2;
  //   // delete[] data3;
  //   // delete[] data4;
  // }

  // const char* data4 = run("5", "5", "{}");
  // delete[] data4;

  // vector container stores threads
  // std::vector<std::thread> workers;
  // for (int i = 1; i <= 4; i++) {
  //   workers.push_back(std::thread(task, 1));
  // }
  //
  // std::for_each(workers.begin(), workers.end(), [](std::thread &t) {
  //     t.join();
  // });
  //
  //
  // clock_gettime(CLOCK_MONOTONIC, &finish);
  // elapsed = (finish.tv_sec - start.tv_sec);
  // elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
  //
  // std::cout << elapsed << std::endl;

  char* error = NULL;
  if (!compile("5", "5", "(function(data) { return data; require('./testlib.js'); data.time = moment().format('MMMM Do YYYY, h:mm:ss a'); return data;})", error)) {
    std::cout << "Error here: " << error << std::endl;
    return 1;
  }

  char* err = NULL;
  char* data2 = run("5", "5", "{\"a\": 1, \"b\": [1,3]}", err);
  if (err) {
    std::cout << "Error: " << err << std::endl;
    delete[] err;
  } else {
    std::cout << "Data: " << data2 << std::endl;
    delete[] data2;
  }

  deinitialize();

  // int a = 0;
  // std::cin >> a;
}

std::string readFile(const char* filename) {
  std::ifstream t(filename);
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}
