/* cnode_s2.c */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <chrono>
#include <sstream>

#include "threadpool.h"

#include "erl_interface.h"
#include "ei.h"

#include "test.h"

#define BUFSIZE 1000

std::mutex g_display_mutex;

int my_listen(int);
int foo(int, int);
int bar(int);

void process(int fd, ErlMessage emsg) {
  // printf("in process");

 std::thread::id this_id = std::this_thread::get_id();
 g_display_mutex.lock();
 std::cout << this_id << std::endl;
 g_display_mutex.unlock();

 std::stringstream ss;
 ss << this_id;

 char* error = NULL;
 if (!compile(ss.str().c_str(), ss.str().c_str(), "(function(data) { data.a += 1; data.b.push(1); return data; })", error)) {
     std::cout << "Error" << std::endl;
    //  return 1;
 }

 const char* data3 = run(ss.str().c_str(), ss.str().c_str(), "{\"a\": 1, \"b\": [1,3]}", error);

  ETERM *fromp = nullptr,
        *tuplep = nullptr,
        *fnp = nullptr,
        *argp = nullptr,
        *argp2 = nullptr,
        *resp = nullptr;

  int res = 0;

  fromp = erl_element(2, emsg.msg);
  // tuplep = erl_element(3, emsg.msg);
  // fnp = erl_element(1, tuplep);
  // argp = erl_element(2, tuplep);
  // argp2 = erl_element(3, tuplep);
  //
  // if (strncmp(ERL_ATOM_PTR(fnp), "foo", 3) == 0) {
  //   res = foo(ERL_INT_VALUE(argp), ERL_INT_VALUE(argp2));
  // } else if (strncmp(ERL_ATOM_PTR(fnp), "bar", 3) == 0) {
  //   res = bar(ERL_INT_VALUE(argp));
  // }

  res = 10;

  resp = erl_format("{cnode, ~i}", res);
  erl_send(fd, fromp, resp);

  erl_free_term(emsg.from); erl_free_term(emsg.msg);
  erl_free_term(fromp); erl_free_term(tuplep);
  erl_free_term(fnp); erl_free_term(argp);
  erl_free_term(resp);

  // std::this_thread::sleep_for (std::chrono::seconds(1));
}

int main(int argc, char **argv) {

  std::string pathToLibs("../"); // absolute path here
  initialize(argc, argv, pathToLibs.c_str());

  struct in_addr addr;                     /* 32-bit IP number of host */
  int port;                                /* Listen port number */
  int listen;                              /* Listen socket */
  int fd;                                  /* fd to Erlang node */
  ErlConnect conn;                         /* Connection data */

  int loop = 1;                            /* Loop flag */
  int got;                                 /* Result of receive */
  unsigned char buf[BUFSIZE];              /* Buffer for incoming message */
  ErlMessage emsg;                         /* Incoming message */

  ETERM *fromp, *tuplep, *fnp, *argp, *resp;
  int res;

  port = atoi(argv[1]);

  erl_init(NULL, 0);

  addr.s_addr = inet_addr("192.168.0.102");
  if (erl_connect_xinit("idril", "cnode", "cnode@192.168.0.102",
			&addr, "secretcookie", 0) == -1)
    erl_err_quit("erl_connect_xinit");

  /* Make a listen socket */
  if ((listen = my_listen(port)) <= 0)
    erl_err_quit("my_listen");

  if (erl_publish(port) == -1)
    erl_err_quit("erl_publish");

  if ((fd = erl_accept(listen, &conn)) == ERL_ERROR)
    erl_err_quit("erl_accept");
  fprintf(stderr, "Connected to %s\n\r", conn.nodename);

  nbsdx::concurrent::ThreadPool<std::function<void(int, ErlMessage)>, 4> pool; // Defaults to 10 threads.

  while (loop) {

    got = erl_receive_msg(fd, buf, BUFSIZE, &emsg);

    if (got == ERL_TICK) {
      /* ignore */
    } else if (got == ERL_ERROR) {
      loop = 0;
    } else {

      if (emsg.type == ERL_REG_SEND) {

        pool.AddJob(process, fd, emsg);

        // std::thread th(process, fd, emsg);
        // th.detach();

      	// fromp = erl_element(2, emsg.msg);
      	// tuplep = erl_element(3, emsg.msg);
      	// fnp = erl_element(1, tuplep);
      	// argp = erl_element(2, tuplep);
        //
      	// if (strncmp(ERL_ATOM_PTR(fnp), "foo", 3) == 0) {
      	//   res = foo(ERL_INT_VALUE(argp));
      	// } else if (strncmp(ERL_ATOM_PTR(fnp), "bar", 3) == 0) {
      	//   res = bar(ERL_INT_VALUE(argp));
      	// }
        //
      	// resp = erl_format("{cnode, ~i}", res);
      	// erl_send(fd, fromp, resp);
        //
      	// erl_free_term(emsg.from); erl_free_term(emsg.msg);
      	// erl_free_term(fromp); erl_free_term(tuplep);
      	// erl_free_term(fnp); erl_free_term(argp);
      	// erl_free_term(resp);
      }
    }
  }
}


int my_listen(int port) {
  int listen_fd;
  struct sockaddr_in addr;
  int on = 1;

  if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return (-1);

  setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  memset((void*) &addr, 0, (size_t) sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(listen_fd, (struct sockaddr*) &addr, sizeof(addr)) < 0)
    return (-1);

  listen(listen_fd, 5);
  return listen_fd;
}
