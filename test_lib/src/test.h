extern "C" {
  void initialize(int, char**, const char*);
  void deinitialize();
  bool compile(const char*, const char*, const char*, char*&);
  char* run(const char*, const char*, const char*, char*&);
}
