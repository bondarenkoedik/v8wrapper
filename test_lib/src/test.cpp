#include <string.h>

#include <iostream>

#include <map>
#include <string>
#include <mutex>

#include <sstream>
#include <fstream>

#include "libplatform/libplatform.h"
#include "v8.h"

#define ISOLATE 0
#define CONTEXT 1
#define FUNCTION 2

// specific v8 data
static v8::Platform* platform = nullptr;
static v8::Isolate::CreateParams create_params;

// data to store compiled scripts
static std::map<
  std::pair<std::string, std::string>,
  std::tuple<v8::Isolate*,
             v8::Persistent<v8::Context, v8::CopyablePersistentTraits<v8::Context>>,
             v8::Persistent<v8::Function, v8::CopyablePersistentTraits<v8::Function>>
  >
> _data;

std::mutex _mutex;

std::string g_pathToLibs;


static void _initialize(int, char**, const char*);
static bool _compile(const char*, const char*, const char*, char*&);
static std::string _run(const char*, const char*, const char*, char*&);
static void _deinitialize();
static void _cleanData();

static std::string jsonStr(v8::Isolate* isolate, v8::Handle<v8::Value>);
static v8::Local<v8::Object> convertToObject(v8::Isolate*, const v8::Local<v8::Object>&);

static void Print(const v8::FunctionCallbackInfo<v8::Value>& args);
static void Require(const v8::FunctionCallbackInfo<v8::Value>& args);
static std::string readFile(const char* filename);

extern "C" void initialize(int argc, char* argv[], const char* pathToLibs) {
  _initialize(argc, argv, pathToLibs);
}

extern "C" void deinitialize() {
  _deinitialize();
}

// don't forget to free error memory if error is not NULL
extern "C" bool compile(const char* conv_id, const char* node_id, const char* src, char*& error) {
  return _compile(conv_id, node_id, src, error);
}

// don't forget to free data memory after run
// don't forget to free error memory if error is not NULL
extern "C" char* run(const char* conv_id, const char* node_id, const char* data, char*& error) {

  std::string ret(_run(conv_id, node_id, data, error));

  if (ret.empty()) {
    return NULL;
  }

  char* ptr = new char[ret.length() + 1];
  strcpy(ptr, ret.c_str());
  return ptr;
}

void _initialize(int argc, char* argv[], const char* pathToLibs) {
  v8::V8::InitializeICUDefaultLocation(argv[0]);
  v8::V8::InitializeExternalStartupData(argv[0]);
  platform = v8::platform::CreateDefaultPlatform();
  v8::V8::InitializePlatform(platform);
  v8::V8::Initialize();

  // const char* v8Flags = "--debugger --expose_debug_as=v8debug";
	// v8::V8::SetFlagsFromString(v8Flags, strlen(v8Flags));

  create_params.array_buffer_allocator = v8::ArrayBuffer::Allocator::NewDefaultAllocator();

  g_pathToLibs = std::string(pathToLibs);
}

void _deinitialize() {

  _cleanData();

  v8::V8::Dispose();
  v8::V8::ShutdownPlatform();

  delete platform;
  delete create_params.array_buffer_allocator;

}

// add check that we already compiled this script
bool _compile(const char* conv_id, const char* node_id, const char* src, char*& error) {

  v8::Isolate* isolate = v8::Isolate::New(create_params);

  {
    v8::Locker locker(isolate);
    v8::Isolate::Scope isolate_scope(isolate);
    v8::HandleScope scope(isolate);

    v8::Local<v8::ObjectTemplate> global = v8::ObjectTemplate::New(isolate);
    global->Set(v8::String::NewFromUtf8(isolate, "print"), v8::FunctionTemplate::New(isolate, Print));
    global->Set(v8::String::NewFromUtf8(isolate, "require"), v8::FunctionTemplate::New(isolate, Require));

    v8::Persistent<v8::Context, v8::CopyablePersistentTraits<v8::Context>> pContext(isolate, v8::Context::New(isolate, NULL, global));
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(isolate, pContext);

    // Enter the context for compiling and running the hello world script.
    v8::Context::Scope context_scope(context);

    auto script = v8::String::NewFromUtf8(isolate, src);

    v8::TryCatch try_catch(isolate);

    // compile

    v8::Local<v8::Script> compiled_script;
    if (!v8::Script::Compile(context, script).ToLocal(&compiled_script)) {
      v8::String::Utf8Value err(try_catch.Exception());
      error = new char[strlen(*err) + 1];
      strcpy(error, *err);
      // printf("Logged: %s\n", *err);
      return false;
    }

    v8::Local<v8::Value> result;
    if (!compiled_script->Run(context).ToLocal(&result)) {
      v8::String::Utf8Value err(try_catch.Exception());
      error = new char[strlen(*err) + 1];
      strcpy(error, *err);
      // printf("Logged: %s\n", *error);
      return false;
    }

    auto key = std::make_pair(std::string(conv_id), std::string(node_id));

    _mutex.lock();

    _data[key] = std::make_tuple(
      isolate,
      pContext,
      v8::Persistent<v8::Function, v8::CopyablePersistentTraits<v8::Function>>(isolate, result.As<v8::Function>())
    );

    _mutex.unlock();
  }

  return true;
}

std::string _run(const char* conv_id, const char* node_id, const char* data, char*& error) {

  auto key = std::make_pair(std::string(conv_id), std::string(node_id));

  if (_data.find(key) == _data.end()) {
    const char* err = "Not found pair (cond_id, nod_id).";
    error = new char[strlen(err) + 1];
    strcpy(error, err);
    return "";
  }

  v8::Isolate* isolate = std::get<ISOLATE>(_data[key]);
  v8::Locker locker(isolate);

  v8::Isolate::Scope isolate_scope(isolate);
  v8::HandleScope scope(isolate);

  v8::TryCatch try_catch(isolate);

  v8::Local<v8::Context> context = v8::Local<v8::Context>::New(isolate, std::get<CONTEXT>(_data[key]));

  // Enter the context for compiling and running the hello world script.
  v8::Context::Scope context_scope(context);

  v8::MaybeLocal<v8::Value> jsonData = v8::JSON::Parse(isolate, v8::String::NewFromUtf8(isolate, data));

  if (jsonData.IsEmpty()) {
    const char* err = "Error during parse JSON.";
    error = new char[strlen(err) + 1];
    strcpy(error, err);
    return "";
  }

  v8::Local<v8::Object> obj = jsonData.ToLocalChecked()->ToObject();

  v8::Local<v8::Value> args[] = { convertToObject(isolate, obj) };

  _mutex.lock();
    v8::Local<v8::Function> func = v8::Local<v8::Function>::New(isolate, std::get<FUNCTION>(_data[key]));
  _mutex.unlock();

  v8::Local<v8::Value> res = func->Call(context->Global(), 1, args);

  if (res.IsEmpty()) {
    v8::String::Utf8Value err(try_catch.Exception());
    error = new char[strlen(*err) + 1];
    strcpy(error, *err);
    return "";
  }

  return jsonStr(isolate, res);
}

// additional functions

// Stringify V8 value to JSON
// return empty string for empty value
std::string jsonStr(v8::Isolate* isolate, v8::Handle<v8::Value> value)
{
  if (value.IsEmpty())
  {
      return std::string();
  }

  v8::Local<v8::Object> json = isolate->GetCurrentContext()->
      Global()->Get(v8::String::NewFromUtf8(isolate, "JSON"))->ToObject();
  v8::Local<v8::Function> stringify = json->Get(v8::String::NewFromUtf8(isolate, "stringify")).As<v8::Function>();

  v8::Local<v8::Value> result = stringify->Call(json, 1, &value);
  const v8::String::Utf8Value str(result);

  return std::string(*str, str.length());
}

v8::Local<v8::Object> convertToObject(v8::Isolate* isolate, const v8::Local<v8::Object>& json) {

  v8::Local<v8::Object> obj = v8::Object::New(isolate);

  v8::MaybeLocal<v8::Array> maybe_props = json->GetOwnPropertyNames(isolate->GetCurrentContext());
  if (!maybe_props.IsEmpty()) {
      v8::Local<v8::Array> props = maybe_props.ToLocalChecked();
      for(uint32_t i=0; i < props->Length(); i++) {
         v8::Local<v8::Value> key = props->Get(i);
         v8::Local<v8::Value> value = json->Get(key);
         obj->Set(key, value);
      }
  }

  return obj;
}

void _cleanData() {
  for (auto& kv: _data) {
    std::get<FUNCTION>(kv.second).Reset();
    std::get<CONTEXT>(kv.second).Reset();
    std::get<ISOLATE>(kv.second)->Dispose();
  }
  _data.clear();
}

void Print(const v8::FunctionCallbackInfo<v8::Value>& args) {

    v8::Isolate *isolate = args.GetIsolate();

    bool first = true;
    for (int i = 0; i < args.Length(); i++) {
        if (first)
            first = false;
        else
            printf(" ");

        if (args[i]->IsObject()) {
          std::cout << jsonStr(isolate, args[i]);
        } else {
          v8::String::Utf8Value str(args[i]);
          printf("%s", *str);
        }
    }

    printf("\n");
}

void Require(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::Isolate *isolate = args.GetIsolate();
  v8::String::Utf8Value str(args[0]);
  std::string libContent = readFile(*str); // avoid using this global in a future
  auto script = v8::String::NewFromUtf8(isolate, libContent.c_str());

  v8::TryCatch try_catch(isolate);

  v8::Local<v8::Script> compiled_script;
  if (!v8::Script::Compile(isolate->GetCurrentContext(), script).ToLocal(&compiled_script)) {
    v8::String::Utf8Value error(try_catch.Exception());
    printf("Logged: %s\n", *error);
    return;
  }

  v8::Local<v8::Value> result;
  if (!compiled_script->Run(isolate->GetCurrentContext()).ToLocal(&result)) {
    v8::String::Utf8Value err(try_catch.Exception());
    printf("Logged: %s\n", *err);
    return;
  }
}

std::string readFile(const char* filename) {
  std::string fullPath = g_pathToLibs + std::string(filename);
  std::ifstream t(fullPath);
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}
