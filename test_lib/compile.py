import os

commands = [
    "g++ -g -c -o ./obj/test.o -Wall -Werror -fpic ./src/test.cpp -I ./src -I ../../include/ -fopenmp",
    "g++ -g -shared -o ./lib/libtest.so ./obj/test.o -fopenmp -L ../../../icu/build/lib -lpthread -licuuc -licui18n -licuio -licule -licudata",
    # "g++ -shared -o ./lib/libv8.so ./v8/*.o -L ../../../icu/build/lib -lpthread -licuuc -licui18n -licuio -licule -licudata",
    "g++ -g ./src/main.c ./v8/*.o -o ./bin/main -fopenmp -L ./lib -L ./v8 -L ../../../icu/build/lib -lpthread -licuuc -licui18n -licuio -licule -licudata -ltest -lv8"

];
# g++ main.cpp -o main ./objs/*.o -I ../include/ -L ../../icu/build/lib -lpthread -licuuc -licui18n -licuio -licule -licudata

for command in commands:
    print(command)
    os.system(command)
