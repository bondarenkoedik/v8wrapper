g++ -c -Wall -Werror -fpic lib.cpp
g++ -shared -o libfoo.so lib.o
gcc -Wall -o test main.c -L . -lfoo

export LD_LIBRARY_PATH=./
